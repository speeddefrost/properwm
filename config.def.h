// macros

#define MODKEY Mod4Mask
#define TERM "urxvt"

#define DRED "#993D3D"
#define LRED "#B23535"
#define DYELLOW "#99993D"
#define LYELLOW "#B2B235"
#define DGREEN "#4C993D"
#define LGREEN "#4AB235"
#define DCYAN "#3D9999"
#define LCYAN "#35B2B2"
#define DBLUE "#3D6399"
#define LBLUE "#3569B2"
#define DPINK "#993D89"
#define LPINK "#B2359D"

#define D1 "#000000"
#define D2 "#262626"
#define D3 "#404040"
#define L1 "#888888"
#define L2 "#CCCCCC"
#define L3 "#E6E6E6"

#define AC "D8"

// commands

char * terminal_cmd[] = { TERM, NULL };
char * browser_cmd[] = { "sh", "-c", "$BROWSER" };

char * menu_cmd[] = {
    "dmenu_run", "-b",
    "-p",  ">> ",
    "-fn", "-*-tamsyn-*-*-*-*-14-*-*-*-*-*-*-*",
    "-nb", D1, "-nf", L2,
    "-sb", DGREEN, "-sf", D1,
    NULL
};

// behavior

int  init_attach_point =    ATTACH_NEXT;
int  init_current_tags[] =  { 0,1 }; // index
int  init_selected_tags[] = { 0,0 }; // bitmask

int  snap_threshold =       4;
bool focus_follows_mouse =  false;
bool tiled_size_hints =     false;

// general appearance

char * panel_font_name =    "Misc Tamsyn";
int    panel_font_size =    12;
bool   panel_visible =      true;
int    panel_position =     TOP;
int    panel_height =       -1;

int    border_width =       2;
int    padding =            4;
int    struts[4] =          { 0,0,0,0 };
int    tag_padding =        6;
int    tag_ix_size =        6;
int    tag_margins[2] =     { 1,1 };

char * counter_format_str = "%i/%i";

int    title_alignment =    ALIGN_CENTER;
int    title_ellipsize =    ELLIPSIZE_END;
char * title_fallback =     "";

// colors

char * border_normal =      L1;
char * border_previous =    L2;
char * border_selected =    L3;
char * border_urgent =      LRED;

char * urgent_tag_bg =      D2 AC;
char * urgent_tag_fg =      LRED;
char * current_tag_bg =     D3 AC;
char * current_tag_fg =     DCYAN;
char * selected_tag_bg =    D3 AC;
char * selected_tag_fg =    L2;
char * used_tag_bg =        D2 AC;
char * used_tag_fg =        L2;
char * unused_tag_bg =      D2 AC;
char * unused_tag_fg =      L1;

char * layout_bg =          D2 AC;
char * layout_fg =          DCYAN;
char * title_bg =           D1 AC;
char * title_fg =           LCYAN;
char * counter_bg =         D1 AC;
char * counter_fg =         DCYAN;
char * status_bg =          D2 AC;
char * status_fg =          L3;

// tags

TagTemplate tags[] = {
//    name,  layout,   nmaster,  mfactor
    { "1",   HTILE,    -1,       0.5 },
    { "2",   MONOCLE,  -1,       0.5 },
    { "3",   VTILE,    -1,       0.5 },
    { "4",   VTILE,    -1,       0.5 },
};

// layout map

Layout layouts[N_LAYOUTS] = {
    [VTILE] =    { "VT", vtile   },
    [HTILE] =    { "HT", htile   },
    [VFOLD] =    { "VF", vfold   },
    [HFOLD] =    { "HV", hfold   },
    [MONOCLE] =  { "MO", monocle },
    [FLOATING] = { "FL", NULL    }
};

// attach symbol map

char* attach_point_sym[] = {
    "+", // head
    "=", // body
    ":", // tail
    "<", // prev
    ">"  // next
};

// client rules

Rule rules[] = {
// xprop(1):
//     WM_CLASS(STRING) = instance, class
//     WM_NAME(STRING) = title
//
// attributes: ADD_TAGS, FLOATING, NO_FOCUS, VIEW

//    class,               instance  title               monitor  attach point  tags     attrs
    { "Chromium",          NULL,     "Chromium",         -1,      ATTACH_HEAD,  BIT(2),  0        },
    { "Vlc",               NULL,     NULL,               -1,      -1,           BIT(2),  0        },
    { "Transmission-gtk",  NULL,     NULL,               -1,      -1,           BIT(2),  0        },
    { "Gcolor3",           NULL,     NULL,               -1,      ATTACH_TAIL,  0,       FLOAT    }
};

// keybindings

#define TAG_KEYS(KEY,ARG) \
    { MODKEY,                       KEY,                set_tags,                   { .ui = ARG } },    \
    { MODKEY|ShiftMask,             KEY,                set_client_tags,            { .ui = ARG } },    \
    { MODKEY|ControlMask,           KEY,                toggle_tags,                { .ui = ARG } },    \
    { MODKEY|ControlMask|ShiftMask, KEY,                toggle_client_tags,         { .ui = ARG } }

Key keys[] = {
    TAG_KEYS(                       XK_1,                                           BIT(1) ),
    TAG_KEYS(                       XK_2,                                           BIT(2) ),
    TAG_KEYS(                       XK_3,                                           BIT(3) ),
    TAG_KEYS(                       XK_4,                                           BIT(4) ),
    TAG_KEYS(                       XK_5,                                           BIT(5) ),
    TAG_KEYS(                       XK_6,                                           BIT(6) ),
    TAG_KEYS(                       XK_7,                                           BIT(7) ),
    TAG_KEYS(                       XK_8,                                           BIT(8) ),
    TAG_KEYS(                       XK_9,                                           BIT(9) ),

    { MODKEY,                       XK_0,               set_tags,                   { .ui = ~0 } },
    { MODKEY|ShiftMask|ControlMask, XK_0,               set_client_tags,            { .ui = ~0 } },
    { MODKEY,                       XK_grave,           toggle_tag_set,             { 0 } },
    { MODKEY,                       XK_Tab,             toggle_focus,               { 0 } },
    { MODKEY|ShiftMask,             XK_Tab,             zoom_swap,                  { 0 } },
    { MODKEY,                       XK_k,               focus_client,               { .i = -1 } },
    { MODKEY,                       XK_j,               focus_client,               { .i = +1 } },
    { MODKEY|ShiftMask,             XK_k,               push,                       { .i = -1 } },
    { MODKEY|ShiftMask,             XK_j,               push,                       { .i = +1 } },
    { MODKEY,                       XK_comma,           focus_monitor,              { .i = -1 } },
    { MODKEY,                       XK_period,          focus_monitor,              { .i = +1 } },
    { MODKEY|ShiftMask,             XK_comma,           client_to_monitor,          { .i = -1 } },
    { MODKEY|ShiftMask,             XK_period,          client_to_monitor,          { .i = +1 } },

    { MODKEY,                       XK_bracketleft,     set_attach_point,           { .i = -1 } },
    { MODKEY,                       XK_bracketright,    set_attach_point,           { .i = +1 } },
    { MODKEY,                       XK_e,               set_layout,                 { .v = &layouts[HFOLD] } },
    { MODKEY,                       XK_d,               set_layout,                 { .v = &layouts[VFOLD] } },
    { MODKEY,                       XK_r,               set_layout,                 { .v = &layouts[HTILE] } },
    { MODKEY,                       XK_t,               set_layout,                 { .v = &layouts[VTILE] } },
    { MODKEY,                       XK_g,               set_layout,                 { .v = &layouts[MONOCLE] } },
    { MODKEY,                       XK_f,               set_layout,                 { .v = &layouts[FLOATING] } },
    { MODKEY,                       XK_h,               set_mfactor,                { .f = -0.025 } },
    { MODKEY,                       XK_l,               set_mfactor,                { .f = +0.025 } },
    { MODKEY,                       XK_m,               set_mfactor,                { .f = 0.0 } },
    { MODKEY|ShiftMask,             XK_h,               set_cfactor,                { .f = -0.025 } },
    { MODKEY|ShiftMask,             XK_l,               set_cfactor,                { .f = +0.025 } },
    { MODKEY|ShiftMask,             XK_m,               set_cfactor,                { .f = 0.0 } },
    { MODKEY,                       XK_z,               set_nmaster,                { .i = -1 } },
    { MODKEY,                       XK_a,               set_nmaster,                { .i = +1 } },
    { MODKEY,                       XK_x,               set_nmaster,                { .f = 0.0 } },
    { MODKEY,                       XK_minus,           set_padding,                { .i = -2 } },
    { MODKEY,                       XK_equal,           set_padding,                { .i = +2 } },

    { MODKEY,                       XK_Return,          spawn,                      { .v = terminal_cmd } },
    { MODKEY|ShiftMask,             XK_b,               spawn,                      { .v = browser_cmd } },
    { MODKEY,                       XK_p,               spawn,                      { .v = menu_cmd } },

    { MODKEY,                       XK_space,           toggle_floating,            { 0 } },
    { MODKEY,                       XK_b,               toggle_panel_visible,       { 0 } },
    { MODKEY,                       XK_v,               toggle_panel_position,      { 0 } },

    { MODKEY|ShiftMask,             XK_q,               kill_client,                { 0 } },
    { MODKEY|ShiftMask,             XK_e,               quit,                       { 0 } },
    { MODKEY|ShiftMask,             XK_r,               restart,                    { 0 } }
};

#define TAG_BUTTONS(ARG) \
    { CLICK_TAG,        0,              Button1,    set_tags,           { .ui = ARG } },    \
    { CLICK_TAG,        0,              Button3,    toggle_tags,        { .ui = ARG } },    \
    { CLICK_TAG,        MODKEY,         Button1,    set_client_tags,    { .ui = ARG } },    \
    { CLICK_TAG,        MODKEY,         Button3,    toggle_client_tags, { .ui = ARG } }

// targets: ClickTag, ClickLayout, ClickTitle, ClickStats, ClickStatus, ClickRoot, ClickClient

Button buttons[] = {
//  target              mask                button          function            argument */
    TAG_BUTTONS(                                                                BIT(1) ),
    TAG_BUTTONS(                                                                BIT(2) ),
    TAG_BUTTONS(                                                                BIT(3) ),
    TAG_BUTTONS(                                                                BIT(4) ),
    TAG_BUTTONS(                                                                BIT(5) ),
    TAG_BUTTONS(                                                                BIT(6) ),
    TAG_BUTTONS(                                                                BIT(7) ),
    TAG_BUTTONS(                                                                BIT(8) ),
    TAG_BUTTONS(                                                                BIT(9) ),
    { CLICK_TITLE,      0,                  Button3,        focus_client,       { .i = -1 } },
    { CLICK_TITLE,      0,                  Button1,        focus_client,       { .i = +1 } },
    { CLICK_TITLE,      MODKEY,             Button1,        zoom_swap,          { 0 } },
    { CLICK_TITLE,      0,                  8,              push,               { .i = -1 } }, // tilt left
    { CLICK_TITLE,      0,                  9,              push,               { .i = +1 } }, // tilt right
    { CLICK_TITLE,      MODKEY,             Button2,        toggle_floating,    { 0 } },
    { CLICK_CLIENT,     MODKEY,             Button1,        move_mouse,         { 0 } },
    { CLICK_CLIENT,     MODKEY,             Button3,        resize_mouse,       { 0 } },
    { CLICK_STATUS,     0,                  Button3,        spawn,              { .v = terminal_cmd } },
};
