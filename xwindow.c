long xwindow_get_state (Window w) {
  int format;
  long result = -1;
  unsigned char *p = NULL;
  unsigned long n;
  unsigned long extra;
  Atom real;

  if(XGetWindowProperty(dpy, w, wmatom[WMState], 0L, 2L, false, wmatom[WMState], &real, &format, &n, &extra, (unsigned char **)&p) != Success)
    return -1;

  if(n != 0)
    result = *p;

  XFree(p);
  return result;
}

bool xwindow_get_text_prop (Window w, Atom atom, char *text, int size) {
  char **list = NULL;
  int n;
  XTextProperty name;

  if(text == NULL || size == 0)
    return false;

  text[0] = '\0';
  XGetTextProperty(dpy, w, &name, atom);

  if(name.nitems == 0)
    return false;

  if(name.encoding == XA_STRING) {
    strncpy(text, (char*) name.value, size - 1);
  }
  else {
    if(XmbTextPropertyToTextList(dpy, &name, &list, &n) >= Success && n > 0 && *list) {
      strncpy(text, *list, size - 1);
      XFreeStringList(list);
    }
  }

  text[size - 1] = '\0';
  XFree(name.value);

  return true;
}

void xwindow_manage (Window w, XWindowAttributes *wa) {
  Client *c,
         *trans;
  Window trans_win;
  bool view;
  int attach_point;
  Tag *t;
  XWindowChanges wc;

  c = malloc(sizeof(Client));
  if(c == NULL)
    die("out of memory\n");

  memset(c, 0, sizeof(Client));

  c->win = w;
  c->cfactor = 1.0;
  client_update_name(c);

  trans_win = None;
  XGetTransientForHint(dpy, c->win, &trans_win);
  trans = xwindow_to_client(trans_win);
  view = false;

  if(trans != NULL) {
    t = ONSCREEN(trans) ? &CURTAGW(trans->mon) : client_first_tag(trans);

    c->mon = trans->mon;
    c->tags = trans->tags;
    c->floating = true;

    attach_point = c->mon->attach_point;
  }
  else {
    t = &CURTAGW(selmon);

    c->mon = selmon;
    c->tags = SELTAGS(c->mon);
    c->floating = false;

    attach_point = c->mon->attach_point;
    client_apply_rules(c, &t, &view, &attach_point);
  }

  c->orig_bw = wa->border_width;
  c->orig_x = wa->x;
  c->orig_y = wa->y;
  c->orig_w = wa->width;
  c->orig_h = wa->height;

  c->fx = c->orig_x;
  c->fy = c->orig_y;
  c->fw = c->orig_w;
  c->fh = c->orig_h;

  if(FL_WIDTH(c) > c->mon->ww)
    c->fw = (c->mon->ww * 0.75) - (c->bw * 2);
  if(FL_HEIGHT(c) > c->mon->wh)
    c->fh = (c->mon->wh * 0.75) - (c->bw * 2);

  if(c->fx <= c->mon->wx)
    c->fx = c->mon->wx + ((c->mon->ww - FL_WIDTH(c)) / 2);
  if(c->fy <= c->mon->wx)
    c->fy = c->mon->wy + ((c->mon->wh - FL_HEIGHT(c)) / 2);

  if(c->fullscreen) {
    c->bw = 0;
    c->x = c->mon->mx;
    c->y = c->mon->my;
    c->w = c->mon->mw;
    c->h = c->mon->mh;
  }
  else {
    c->bw = border_width;
    c->x = c->fx;
    c->y = c->fy;
    c->w = c->fw;
    c->h = c->fh;
  }

  wc.border_width = c->bw;
  XConfigureWindow(dpy, c->win, CWBorderWidth, &wc);
  XSetWindowBorder(dpy, c->win, colors.border_normal);

  client_update_window_type(c);
  client_update_size_hints(c);
  client_update_wm_hints(c);

  XSelectInput(dpy, c->win, EnterWindowMask|FocusChangeMask|PropertyChangeMask|StructureNotifyMask);
  grab_buttons(c, false);

  if(c->floating == false) {
    c->floating = trans != None || c->fixed;
    c->was_floating = c->floating;
  }

  client_configure(c);

  attach(c, t, attach_point);
  attach_stack(c);

  XChangeProperty(dpy, root, netatom[NetClientList], XA_WINDOW, 32, PropModeAppend, (unsigned char*) &w, 1);
  XMoveResizeWindow(dpy, c->win, c->x, c->y, c->w, c->h);

  client_set_state(c, NormalState);

  if(view && ONSCREEN(c) == false) {
    CURTAG(c->mon) = t->index;
    SELTAGS(c->mon) |= (1 << t->index);
  }

  if(c->floating == false)
    XLowerWindow(dpy, c->win);

  arrange(c->mon);
  XMapWindow(dpy, c->win);
  focus(NULL);

  panel_refresh(c->mon);
}
