#include "properwm.h"

void grab_buttons (Client *c, bool focused) {
  int i,j;
  unsigned int modifiers[] = {
    0,
    LockMask,
    numlockmask,
    numlockmask | LockMask
  };

  update_numlock_mask();
  XUngrabButton(dpy, AnyButton, AnyModifier, c->win);

  if(focused) {
    for(i = 0; i < LENGTH(buttons); ++i) {
      if(buttons[i].click == CLICK_CLIENT) {
        for(j = 0; j < LENGTH(modifiers); j++) {
          XGrabButton(dpy, buttons[i].button,
              buttons[i].mask | modifiers[j],
              c->win, false, BUTTON_MASK,
              GrabModeAsync, GrabModeSync, None, None);
        }
      }
    }
  }
  else {
    XGrabButton(dpy, AnyButton, AnyModifier, c->win, false,
        BUTTON_MASK, GrabModeAsync, GrabModeSync, None, None);
  }
}

void set_focus (Client *c) {
  if(c->never_focus == false) {
    XSetInputFocus(dpy, c->win, RevertToPointerRoot, CurrentTime);
    XChangeProperty(dpy, root, netatom[NetActiveWindow],
        XA_WINDOW, 32, PropModeReplace,
        (unsigned char *) &c->win, 1);
  }

  client_send_event(c, wmatom[WMTakeFocus]);
}

void reset_focus (void) {
  XSetInputFocus(dpy, root, RevertToPointerRoot, CurrentTime);
  XDeleteProperty(dpy, root, netatom[NetActiveWindow]);
}

void unfocus (Client *c) {
  grab_buttons(c, false);
  XSetWindowBorder(dpy, c->win, colors.border_normal);
}

void focus (Client *c) {
  if(c == NULL || ONSCREEN(c) == false) {
    for(c = selmon->stack; c != NULL; c = c->snext) {
      if(ONSCREEN(c))
        break;
    }
  }

  if(c == selmon->selected)
    return;

  if(selmon->selected != NULL)
    unfocus(selmon->selected);

  if(c != NULL) {
    if(c->mon != selmon) {
      Monitor *oldmon = selmon;
      selmon = c->mon;
      panel_refresh(oldmon);
      show_hide_status();
    }

    if(c->urgent)
      client_set_urgent(c, false);

    XSetWindowBorder(dpy, c->win, colors.border_selected);
    if(c->floating || CURTAGW(c->mon).layout->func == NULL)
      XRaiseWindow(dpy, c->win);

    detach_stack(c);
    attach_stack(c);

    grab_buttons(c, true);
    set_focus(c);
  }
  else {
    reset_focus();
  }

  CURTAGW(selmon).selected = c;
  selmon->prev = selmon->selected;
  selmon->selected = c;
}

// --

void client_apply_rules (Client *c, Tag **tag, bool *view, int *attach_point) {
  Tag *t = *tag;

  XClassHint ch = { NULL, NULL };
  XGetClassHint(dpy, c->win, &ch);

  int i;
  const Rule *r;
  Monitor *m;

  for(i = 0; i < LENGTH(rules); ++i) {
    r = &rules[i];

    if((r->class == NULL || (ch.res_class != NULL && strstr(ch.res_class, r->class))) &&
        (r->title == NULL || strstr(c->name, r->title)) &&
        (r->instance == NULL || (ch.res_name != NULL && strstr(ch.res_name, r->instance))))
    {
      if(c->floating == false && (r->attrs & FLOAT))
        c->floating = true;

      if(r->monitor >= 0) {
        for(m = mons; m != NULL; m = m->next) {
          if(m->index == r->monitor) {
            c->mon = m;
            break;
          }
        }
      }

      if(r->tags & TAG_MASK) {
        c->tags = r->tags;

        if(r->attrs & ADD_TAGS)
          c->tags |= SELTAGS(c->mon);

        if((c->tags & (1 << t->index)) == 0) {
          t = client_first_tag(c);
          *attach_point = c->mon->attach_point;
        }
      }

      if(r->attrs & VIEW)
        *view = true;

      if(r->attach_point >= 0)
        *attach_point = r->attach_point;

      break;
    }
  }

  *tag = t;

  if(ch.res_class != NULL)
    XFree(ch.res_class);

  if(ch.res_name != NULL)
    XFree(ch.res_name);
}

void client_apply_size_hints (Client *c, int *x, int *y, int *w, int *h) {
  // see last two sentences in ICCCM 4.1.2.3

  bool base_is_minimum = c->base_w == c->min_w && c->base_h == c->min_h;
  if(base_is_minimum == false) {
    *w -= c->base_w;
    *h -= c->base_h;
  }

  // adjust for aspect limits

  if(c->min_a > 0 && c->max_a > 0) {
    if(c->max_a < (float) *w / *h)
      *w = *h *c->max_a + 0.5;
    else if(c->min_a < (float) *h / *w)
      *h = *w *c->min_a + 0.5;
  }

  // increment calculations require this

  if(base_is_minimum) {
    *w -= c->base_w;
    *h -= c->base_h;
  }

  // adjust for increment value

  if(c->inc_w > 0)
    *w -= *w % c->inc_w;

  if(c->inc_h > 0)
    *h -= *h % c->inc_h;

  // restore base dimensions

  *w += c->base_w;
  *h += c->base_h;

  if(*w < c->min_w)
    *w = c->min_w;
  if(*h < c->min_h)
    *h = c->min_h;

  if(c->max_w > 0 && *w > c->max_w)
    *w = c->max_w;
  if(c->max_h > 0 && *h > c->max_h)
    *h = c->max_h;
}

void client_configure (Client *c) {
  XConfigureEvent ce;

  ce.type = ConfigureNotify;
  ce.display = dpy;
  ce.event = c->win;
  ce.window = c->win;
  ce.x = c->x;
  ce.y = c->y;
  ce.width = c->w;
  ce.height = c->h;
  ce.border_width = c->bw;
  ce.above = None;
  ce.override_redirect = false;

  XSendEvent(dpy, c->win, false, StructureNotifyMask, (XEvent*) &ce);
}

Tag *client_first_tag (Client *c) {
  int i;
  for(i = 0; i < c->mon->n_tags; ++i) {
    if(c->tags & (1 << i))
      return &c->mon->tags[i];
  }
  return NULL;
}

Atom client_get_atom_prop (Client *c, Atom prop) {
  int di;
  unsigned long dl;
  unsigned char *p = NULL;
  Atom da;
  Atom atom = None;

  if(XGetWindowProperty(dpy, c->win, prop, 0L, sizeof(atom), false, XA_ATOM, &da, &di, &dl, &dl, &p) == Success && p) {
    memcpy(&atom, p, sizeof(Atom));
    XFree(p);
  }

  return atom;
}

void client_move_to_monitor (Client *c, Monitor *m) {
  detach(c);
  detach_stack(c);
  arrange(c->mon);

  c->mon = m;
  c->tags = 1 << CURTAGW(m).index;

  Tag *nt = &CURTAGW(c->mon);
  attach(c, nt, c->mon->attach_point);
  attach_stack(c);
  arrange(m);

  focus(NULL);
}

void client_resize (Client *c, int x, int y, int w, int h) {
  if(c->fullscreen == false && (c->floating || CURTAGW(c->mon).layout->func == NULL || tiled_size_hints))
    client_apply_size_hints(c, &x, &y, &w, &h);

  c->x = x;
  c->y = y;
  c->w = MAX(w, c->min_w);
  c->h = MAX(h, c->min_h);

  XMoveResizeWindow(dpy, c->win, c->x, c->y, c->w, c->h);
}

bool client_send_event (Client *c, Atom proto) {
  int n;
  Atom *protocols;
  bool exists = false;
  XEvent ev;

  if(XGetWMProtocols(dpy, c->win, &protocols, &n)) {
    while(exists == false && n--)
      exists = protocols[n] == proto;
    XFree(protocols);
  }

  if(exists) {
    ev.type = ClientMessage;
    ev.xclient.window = c->win;
    ev.xclient.message_type = wmatom[WMProtocols];
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = proto;
    ev.xclient.data.l[1] = CurrentTime;
    XSendEvent(dpy, c->win, false, NoEventMask, &ev);
  }

  return exists;
}

void client_set_border_width (Client *c, int border_width) {
  c->bw = border_width;
  XWindowChanges wc = { .border_width = c->bw };
  XConfigureWindow(dpy, c->win, CWBorderWidth, &wc);
}

void client_set_fullscreen (Client *c, bool fullscreen) {
  if(fullscreen) {
    XChangeProperty(dpy, c->win, netatom[NetWMState], XA_ATOM, 32,
                    PropModeReplace, (unsigned char *) &netatom[NetWMFullscreen], 1);

    c->was_floating = c->floating;
    c->floating = true;
    c->fullscreen = true;

    client_set_border_width(c,0);
    client_resize(c, c->mon->mx, c->mon->my, c->mon->ww, c->mon->wh);

    XRaiseWindow(dpy, c->win);
  }
  else {
    XChangeProperty(dpy, c->win, netatom[NetWMState], XA_ATOM, 32,
                    PropModeReplace, (unsigned char *) 0, 0);

    client_set_border_width(c, c->orig_bw);

    c->fullscreen = false;
    c->floating = c->was_floating;
    c->was_floating = false;

    arrange(c->mon);
  }
}

void client_set_state (Client *c, long state) {
  long data[] = { state, None };
  XChangeProperty(dpy, c->win, wmatom[WMState], wmatom[WMState], 32,
      PropModeReplace, (unsigned char *) data, 2);
}

void client_set_tags (Client *c, unsigned int tm) {
  tm &= TAG_MASK;
  if(tm == 0)
    return;

  c->tags = tm;
  arrange(c->mon);

  if(c->mon == selmon && ONSCREEN(c) == false)
    focus(NULL);
}

void client_set_urgent (Client *c, bool urgent) {
  XWMHints *wmh;
  long border;

  wmh = XGetWMHints(dpy, c->win);
  if(wmh == NULL)
    return;

  if(urgent) {
    wmh->flags |= XUrgencyHint;
    XSetWindowBorder(dpy, c->win, colors.border_urgent);
  }
  else {
    wmh->flags &= ~XUrgencyHint; // clear urgency bit, but preserve others

    if(c == selmon->selected)
      border = colors.border_selected;
    else
      border = colors.border_normal;

    XSetWindowBorder(dpy, c->win, border);
  }

  c->urgent = urgent;
  XSetWMHints(dpy, c->win, wmh);
  XFree(wmh);
}

void client_toggle_tags (Client *c, unsigned int tm) {
  tm &= TAG_MASK;
  if(tm == 0 || c->tags == tm)
    return;

  c->tags ^= tm;
  arrange(c->mon);

  if(c->mon == selmon && ONSCREEN(c) == false)
    focus(NULL);
}

void client_unmanage (Client *c, bool destroyed) {
  detach(c);
  detach_stack(c);

  update_client_list();

  if(c->mon->selected == c)
    c->mon->selected = NULL;

  int i;
  for(i = 0; i < c->mon->n_tags; ++i) {
    if(c->mon->tags[i].selected == c)
      c->mon->tags[i].selected = NULL;
  }

  if(destroyed == false) {
    XWindowChanges wc = {
      .x = c->orig_x,
      .y = c->orig_y,
      .width = c->orig_w,
      .height = c->orig_h,
      .border_width = c->orig_bw
    };

    XGrabServer(dpy);
    XSetErrorHandler(xerror_dummy);
    XConfigureWindow(dpy, c->win, CWX|CWY|CWWidth|CWHeight|CWBorderWidth, &wc);
    XUngrabButton(dpy, AnyButton, AnyModifier, c->win);
    client_set_state(c, WithdrawnState);
    XSync(dpy, false);
    XSetErrorHandler(xerror);
    XUngrabServer(dpy);
  }

  arrange(c->mon);
  focus(NULL);
  panel_refresh(c->mon);

  free(c);
}

void client_update_name (Client *c) {
  if(xwindow_get_text_prop(c->win, netatom[NetWMName], c->name, sizeof(c->name)) == 0)
    xwindow_get_text_prop(c->win, XA_WM_NAME, c->name, sizeof(c->name));
}

void client_update_size_hints (Client *c) {
  XSizeHints size;
  long msize;

  if(XGetWMNormalHints(dpy, c->win, &size, &msize) == false)
    size.flags = PSize;

  if(size.flags & PBaseSize) {
    c->base_w = size.base_width;
    c->base_h = size.base_height;
  }
  else if(size.flags & PMinSize) {
    c->base_w = size.min_width;
    c->base_h = size.min_height;
  }
  else {
    c->base_w = 0;
    c->base_h = 0;
  }

  if(size.flags & PResizeInc) {
    c->inc_w = size.width_inc;
    c->inc_h = size.height_inc;
  }
  else {
    c->inc_w = 0;
    c->inc_h = 0;
  }

  if(size.flags & PMaxSize) {
    c->max_w = size.max_width;
    c->max_h = size.max_height;
  }
  else {
    c->max_w = 0;
    c->max_h = 0;
  }

  if(size.flags & PMinSize) {
    c->min_w = size.min_width;
    c->min_h = size.min_height;
  }
  else if(size.flags & PBaseSize) {
    c->min_w = size.base_width;
    c->min_h = size.base_height;
  }
  else {
    c->min_w = 0;
    c->min_h = 0;
  }

  if(size.flags & PAspect) {
    c->min_a = (float) size.min_aspect.y / size.min_aspect.x;
    c->max_a = (float) size.max_aspect.x / size.max_aspect.y;
  }
  else {
    c->max_a = 0;
    c->min_a = 0.0;
  }

  c->fixed = c->max_w && c->max_h && c->min_w && c->min_h && c->max_w == c->min_w && c->max_h == c->min_h;
}

void client_update_window_type (Client *c) {
  Atom state = client_get_atom_prop(c, netatom[NetWMState]);
  if(state == netatom[NetWMFullscreen])
    client_set_fullscreen(c, true);

  Atom wtype = client_get_atom_prop(c, netatom[NetWMWindowType]);
  if(wtype == netatom[NetWMWindowTypeDialog]) {
    if(c->fullscreen)
      c->was_floating = true;
    else
      c->floating = true;
  }
}

void client_update_wm_hints (Client *c) {
  XWMHints *wmh = XGetWMHints(dpy, c->win);
  if(wmh == NULL)
    return;

  if(wmh->flags & XUrgencyHint) {
    if(c == selmon->selected) {
      c->urgent = false;
      wmh->flags &= ~XUrgencyHint;
      XSetWMHints(dpy, c->win, wmh);
    }
    else {
      c->urgent = true;
      XSetWindowBorder(dpy, c->win, colors.border_urgent);
      panel_refresh(c->mon);
    }
  }
  else {
    c->urgent = false;
  }

  if(wmh->flags & InputHint)
    c->never_focus = wmh->input == false;
  else
    c->never_focus = false;

  XFree(wmh);
}

// --

void attach (Client *c, Tag *t, int point) {
  if(point == ATTACH_HEAD) {
    attach_head(c);
  }
  else if(point == ATTACH_TAIL) {
    attach_tail(c);
  }
  else if(point == ATTACH_BODY) {
    attach_body(c,t);
  }
  else {
    Client *s = nvs_masked(t->selected, 1 << t->index);
    if(point == ATTACH_PREV)
      attach_prev(c,s);
    else // point == NEXT
      attach_next(c,s);
  }
}

void attach_head (Client *c) {
  c->prev = NULL;
  c->next = c->mon->clients;

  if(c->mon->clients != NULL)
    c->mon->clients->prev = c;

  c->mon->clients = c;
}

void attach_body (Client *c, Tag *t) {
  Client* (*iter_func) (Client*, unsigned int);

  if(t->layout->func != NULL)
    iter_func = nv_tiled_masked;
  else
    iter_func = nv_masked;

  int n = 1;
  unsigned int tm = (1 << t->index);
  Client *mt = iter_func(c->mon->clients, tm);

  while(mt != NULL && n++ < t->nmaster)
    mt = nv_tiled_masked(mt->next, tm);

  if(mt != NULL)
    attach_prev(c, mt);
  else
    attach_tail(c);
}

void attach_tail (Client *c) {
  if(c->mon->clients == NULL) {
    c->prev = NULL;
    c->mon->clients = c;
  }
  else {
    Client *t;
    for(t = c->mon->clients; t != NULL; t = t->next) {
      if(t->next == NULL) {
        c->prev = t;
        t->next = c;
        break;
      }
    }
  }

  c->next = NULL;
}

void attach_prev (Client *c, Client *s) {
  if(s == NULL) {
    attach_head(c);
    return;
  }

  c->next = s;
  c->prev = s->prev;

  if(s->prev != NULL)
    s->prev->next = c;
  else
    c->mon->clients = c;

  s->prev = c;
}

void attach_next (Client *c, Client *s) {
  if(s == NULL) {
    attach_tail(c);
    return;
  }

  c->prev = s;
  c->next = s->next;

  if(s->next != NULL)
    s->next->prev = c;

  s->next = c;
}

void detach (Client *c) {
  if(c->next != NULL)
    c->next->prev = c->prev;

  if(c->prev != NULL)
    c->prev->next = c->next;
  else
    c->mon->clients = c->next;
}

void attach_stack (Client *c) {
  c->sprev = NULL;
  c->snext = c->mon->stack;

  if(c->mon->stack != NULL)
    c->mon->stack->sprev = c;

  c->mon->stack = c;
}

void detach_stack (Client *c) {
  if(c->snext != NULL)
    c->snext->sprev = c->sprev;

  if(c->sprev != NULL)
    c->sprev->snext = c->snext;
  else
    c->mon->stack = c->snext;
}

// --

int clients_m_count (Client *c, unsigned int tm) {
  int nm = 0;
  while(c != NULL) {
    if(c->tags & tm)
      ++nm;

    c = c->next;
  }

  return nm;
}

int clients_mt_count (Client *c, unsigned int tm) {
  int nmt = 0;
  while(c != NULL) {
    if((c->tags & tm) && c->floating == false)
      ++nmt;

    c = c->next;
  }

  return nmt;
}

int clients_mf_count (Client *c, unsigned int tm) {
  int nmf = 0;
  while(c != NULL) {
    if((c->tags & tm) && c->floating)
      ++nmf;

    c = c->next;
  }

  return nmf;
}

int clients_v_count (Client *c) {
  int nv = 0;
  while(c != NULL) {
    if(ONSCREEN(c))
      ++nv;

    c = c->next;
  }

  return nv;
}

int clients_vt_count (Client *c) {
  int nt = 0;
  while(c != NULL) {
    if(ONSCREEN(c) && c->floating == false)
      ++nt;

    c = c->next;
  }

  return nt;
}

int clients_vf_count (Client *c) {
  int nf = 0;
  while(c != NULL) {
    if(ONSCREEN(c) && c->floating)
      ++nf;

    c = c->next;
  }

  return nf;
}

// --

Client * last_visible (void) {
  Client *l = NULL;
  Client *c = selmon->clients;

  while(c != NULL) {
    if(ONSCREEN(c))
      l = c;

    c = c->next;
  }

  return l;
}

Client * next_visible (Client *c) {
  while(c != NULL) {
    if(ONSCREEN(c))
      return c;

    c = c->next;
  }

  return NULL;
}

Client * next_visible_stack (Client *c) {
  while(c != NULL) {
    if(ONSCREEN(c))
      break;

    c = c->snext;
  }

  return c;
}

Client * nv_masked (Client *c, unsigned int tm) {
  while(c != NULL) {
    if(c->tags & tm)
      return c;

    c = c->next;
  }

  return NULL;
}

Client * nvs_masked (Client *c, unsigned int tm) {
  while(c != NULL) {
    if(c->tags & tm)
      return c;

    c = c->snext;
  }

  return NULL;
}

Client * nv_floating (Client *c) {
  while(c != NULL) {
    if(ONSCREEN(c) && c->floating)
      return c;

    c = c->next;
  }

  return NULL;
}

Client * nv_tiled (Client *c) {
  while(c != NULL) {
    if(ONSCREEN(c) && c->floating == false)
      return c;

    c = c->next;
  }

  return NULL;
}

Client * nv_tiled_masked (Client *c, unsigned int tm) {
  while(c != NULL) {
    if(c->floating == false && (c->tags & tm))
      return c;
    c = c->next;
  }

  return NULL;
}

Client * nv_floating_masked (Client *c, unsigned int tm) {
  while(c != NULL) {
    if(c->floating && (c->tags & tm))
      return c;

    c = c->next;
  }

  return NULL;
}

Client * prev_visible (Client *c) {
  while(c != NULL && ONSCREEN(c) == false)
    c = c->prev;

  return c;
}

Client * pv_floating (Client *c) {
  while(c != NULL) {
    if(ONSCREEN(c) && c->floating)
      break;
    c = c->prev;
  }

  return c;
}

Client * pv_tiled (Client *c) {
  while(c != NULL) {
    if(ONSCREEN(c) && c->floating == false)
      break;
    c = c->prev;
  }

  return c;
}

Client * xwindow_to_client (Window w) {
  Monitor *m;
  Client *c;

  for(m = mons; m; m = m->next) {
    for(c = m->clients; c; c = c->next) {
      if(c->win == w)
        return c;
    }
  }

  return NULL;
}
