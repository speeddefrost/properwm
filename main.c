#include <linux/limits.h>
#include <unistd.h>

bool _restart = false;

int xerror_dummy (Display *dpy, XErrorEvent *ee) {
  return 0;
}

int xerror (Display *dpy, XErrorEvent *ee) {
  if(ee->error_code == BadWindow ||
      (ee->request_code == X_SetInputFocus && ee->error_code == BadMatch) ||
      (ee->request_code == X_PolyText8 && ee->error_code == BadDrawable) ||
      (ee->request_code == X_PolyFillRectangle && ee->error_code == BadDrawable) ||
      (ee->request_code == X_PolySegment && ee->error_code == BadDrawable) ||
      (ee->request_code == X_ConfigureWindow && ee->error_code == BadMatch) ||
      (ee->request_code == X_GrabButton && ee->error_code == BadAccess) ||
      (ee->request_code == X_GrabKey && ee->error_code == BadAccess) ||
      (ee->request_code == X_CopyArea && ee->error_code == BadDrawable))
  {
    return 0;
  }

  fprintf(stderr, "ProperWM error\nrequest = %i, error code = %i\n",
      ee->request_code, ee->error_code);

  return xerrorxlib(dpy, ee);
}

// --

void properwm_init (void) {
  loft_init();
  loft_init_font(panel_font_name, panel_font_size);

  dpy = loft.display;
  root = loft.root;

  screen = loft.screen;
  scr_width = DisplayWidth(dpy, screen);
  scr_height = DisplayHeight(dpy, screen);

  colors.border_normal = get_color(border_normal);
  colors.border_previous = get_color(border_previous);
  colors.border_selected = get_color(border_selected);
  colors.border_urgent = get_color(border_urgent);

  cursor[CursorNormal] = XCreateFontCursor(dpy, XC_left_ptr);
  cursor[CursorResize] = XCreateFontCursor(dpy, XC_sizing);
  cursor[CursorMove] = XCreateFontCursor(dpy, XC_fleur);

  netatom[NetActiveWindow] = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", false);
  netatom[NetSupported] = XInternAtom(dpy, "_NET_SUPPORTED", false);
  netatom[NetWMName] = XInternAtom(dpy, "_NET_WM_NAME", false);
  netatom[NetWMState] = XInternAtom(dpy, "_NET_WM_STATE", false);
  netatom[NetWMFullscreen] = XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", false);
  netatom[NetWMWindowType] = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE", false);
  netatom[NetWMWindowTypeDialog] = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_DIALOG", false);
  netatom[NetClientList] = XInternAtom(dpy, "_NET_CLIENT_LIST", false);

  wmatom[WMProtocols] = XInternAtom(dpy, "WM_PROTOCOLS", false);
  wmatom[WMDelete] = XInternAtom(dpy, "WM_DELETE_WINDOW", false);
  wmatom[WMState] = XInternAtom(dpy, "WM_STATE", false);
  wmatom[WMTakeFocus] = XInternAtom(dpy, "WM_TAKE_FOCUS", false);

  XSetWindowAttributes wa = {
    .cursor = cursor[CursorNormal],
    .event_mask = ButtonPressMask | PropertyChangeMask |
      EnterWindowMask | LeaveWindowMask | PointerMotionMask |
      StructureNotifyMask | SubstructureNotifyMask |
      SubstructureRedirectMask
  };

  XChangeWindowAttributes(dpy, root, CWCursor|CWEventMask, &wa);
  XSelectInput(dpy, root, wa.event_mask);

  XChangeProperty(dpy, root, netatom[NetSupported], XA_ATOM, 32, PropModeReplace, (unsigned char *) netatom, NetLast);
  XDeleteProperty(dpy, root, netatom[NetClientList]);

  setup_monitors();
  update_panels();
  refresh_status();
  show_hide_status();
  grab_keys();

  focus(NULL);
  arrange(NULL);
}

void scan_clients (void) {
  int i;
  unsigned int nw;
  Window d1, d2;
  Window *wins;
  XWindowAttributes wa;

  if(XQueryTree(dpy, root, &d1, &d2, &wins, &nw)) {
    for(i=0; i < nw; ++i) {
      if(XGetWindowAttributes(dpy, wins[i], &wa) == false)
        continue;

      if(wa.map_state == IsViewable && wa.override_redirect == false)
        xwindow_manage(wins[i], &wa);
    }

    XFree(wins);
  }
}

void cleanup (void) {
  Monitor *m;
  while(mons != NULL) {
    m = mons;
    mons = m->next;

    Client *c;
    while(m->clients != NULL) {
      c = m->clients;
      detach(c);
      detach_stack(c);
      free(c);
    }

    free(m->panel);
    free(m);
  }

  XUngrabKey(dpy, AnyKey, AnyModifier, root);
  XSetInputFocus(dpy, PointerRoot, RevertToPointerRoot, CurrentTime);
  XDeleteProperty(dpy, root, netatom[NetActiveWindow]);
  XDeleteProperty(dpy, root, netatom[NetClientList]);
}

void die (const char *errstr, ...) {
  va_list ap;
  va_start(ap, errstr);
  vfprintf(stderr, errstr, ap);
  va_end(ap);
  exit(1);
}

void pre_hook (XEvent *ev) {
  if(ev->type < LASTEvent && handlers[ev->type] != NULL)
    handlers[ev->type](ev);
}

int main (int argc, char **argv) {
  properwm_init();
  scan_clients();
  loft_main(&pre_hook);
  cleanup();

  if(_restart) {
    char path[PATH_MAX];
    char *argr[] = { path, NULL };
    readlink("/proc/self/exe", path, PATH_MAX);
    execvp(path, argr);
  }

  return 0;
}
