#include <math.h>
#include <pango/pangocairo.h>

inline static
void _panel_refresh (Monitor *m) {
  loft.no_refresh = true;
  panel_sync_tags(m);
  panel_sync_layout(m);
  panel_sync_title(m);
  panel_sync_counter(m);
  loft.no_refresh = false;
  loft_queue_refresh(&m->panel->window.w, true);
}

static
int panel_on_change (LoftWidget *w, void *arg, Monitor *m) {
  int panel_y;
  if(m->panel->position == TOP)
    panel_y = m->my;
  else
    panel_y = m->my + m->mh - w->height;

  loft_window_move((LoftWindow*) w, m->mx, panel_y);
  monitor_set_strut(m, m->panel->position, w->visible ? w->height : 0);

  return 0;
}

static
int tag_label_on_destroy (TagLabel *tl, void *arg, void *data) {
  g_object_unref(tl->_lt);
  return 0;
}

inline static
void draw_arrow (cairo_t *cr, LoftWidget *tw, int pos) {
  double x[2],
  y[2];

  if(pos == 1) {
    x[0] = tag_margins[0];
    y[0] = tag_margins[1] + tag_ix_size;
    x[1] = tag_margins[0] + tag_ix_size;
    y[1] = tag_margins[1];

    cairo_move_to(cr, x[0], y[0]);
    cairo_line_to(cr, x[0], y[1]);
    cairo_line_to(cr, x[1], y[1]);
    cairo_line_to(cr, x[0], y[0]);
  }
  else if(pos == 2) {
    x[0] = tw->width - tag_margins[0] - tag_ix_size;
    y[0] = tag_margins[1];
    x[1] = tw->width - tag_margins[0];
    y[1] = tag_margins[1] + tag_ix_size;

    cairo_move_to(cr, x[0], y[0]);
    cairo_line_to(cr, x[1], y[0]);
    cairo_line_to(cr, x[1], y[1]);
    cairo_line_to(cr, x[0], y[0]);
  }
  else if(pos == 3) {
    x[0] = tag_margins[0];
    y[0] = tw->height - tag_margins[1] - tag_ix_size;
    x[1] = tag_margins[0] + tag_ix_size;
    y[1] = tw->height - tag_margins[1];

    cairo_move_to(cr, x[0], y[0]);
    cairo_line_to(cr, x[0], y[1]);
    cairo_line_to(cr, x[1], y[1]);
    cairo_line_to(cr, x[0], y[0]);
  }
  else {
    x[0] = tw->width - tag_margins[0] - tag_ix_size;
    y[0] = tw->height - tag_margins[1];
    x[1] = tw->width - tag_margins[0];
    y[1] = tw->height - tag_margins[1] - tag_ix_size;

    cairo_move_to(cr, x[0], y[0]);
    cairo_line_to(cr, x[1], y[0]);
    cairo_line_to(cr, x[1], y[1]);
    cairo_line_to(cr, x[0], y[0]);
  }

  cairo_fill(cr);
}

static int tag_label_on_draw (TagLabel *tl, cairo_t *cr, Monitor *m) {
  Tag *t = tl->tag;
  LoftWidget *tw = &tl->w;

  struct rgba *bg = NULL,
              *fg = NULL;

  if(t->state & STATE_URGENT) {
    bg = &tl->colors.urgent.bg;
    fg = &tl->colors.urgent.fg;
  }
  else if(t->state & STATE_CURRENT) {
    bg = &tl->colors.current.bg;
    fg = &tl->colors.current.fg;
  }
  else if(t->state & STATE_SELECTED) {
    bg = &tl->colors.selected.bg;
    fg = &tl->colors.selected.fg;
  }
  else if(t->state & STATE_USED) {
    bg = &tl->colors.used.bg;
    fg = &tl->colors.used.fg;
  }
  else {
    bg = &tl->colors.unused.bg;
    fg = &tl->colors.unused.fg;
  }

  rgba_cairo_set(cr, bg);
  cairo_rectangle(cr, 0, 0, tw->width, tw->height);
  cairo_fill(cr);

  rgba_cairo_set(cr, fg);
  cairo_set_line_width(cr, 1.0);
  cairo_move_to(cr, (tw->width - tl->_ext[0]) / 2, (tw->height - tl->_ext[1]) / 2);
  pango_cairo_update_layout(cr, tl->_lt);
  pango_cairo_show_layout(cr, tl->_lt);

  if(t->state & STATE_INDICATE) {
    if(m->panel->position == TOP)
      draw_arrow(cr, tw, 4);
    else
      draw_arrow(cr, tw, 2);
  }

  return 0;
}

// --

Panel * panel_create (Monitor *m) {
  Panel *p = malloc(sizeof(Panel));
  if(p == NULL)
    die("out of memory");

  p->tags = malloc(sizeof(TagLabel) * m->n_tags);
  if(p->tags == NULL)
    die("out of memory");

  p->position = panel_position;

  loft_window_init(&p->window, "properwm-panel", 0, true);
  loft_window_set_constraints(&p->window, CONSTRAIN_H);
  loft_widget_set_drawable(&p->window.w, false);

  loft_layout_init(&p->lt_horiz, ASPECT_H, 0);
  loft_layout_init(&p->lt_tags, ASPECT_H, 0);

  int i;
  TagLabel *tl;
  LoftWidget *tw;
  cairo_t *cr;

  int min_w,
      min_h,
      pref;

  for(i = 0; i < m->n_tags; ++i) {
    tl = &p->tags[i];
    tw = &tl->w;

    memset(&tl->colors, 0, sizeof(tl->colors));

    tl->tag = &m->tags[i];
    tl->tag->label = tl;

    loft_widget_init(tw, TAG_LABEL, true, true, SIZE_STATIC);
    loft_connect(&tw->obj, "draw", tag_label_on_draw, m);

    loft_widget_set_attrs(tw, EXPAND_Y);

    rgba_from_str(&tl->colors.unused.bg, unused_tag_bg);
    rgba_from_str(&tl->colors.unused.fg, unused_tag_fg);

    rgba_from_str(&tl->colors.used.bg, used_tag_bg);
    rgba_from_str(&tl->colors.used.fg, used_tag_fg);

    rgba_from_str(&tl->colors.current.bg, current_tag_bg);
    rgba_from_str(&tl->colors.current.fg, current_tag_fg);

    rgba_from_str(&tl->colors.selected.bg, selected_tag_bg);
    rgba_from_str(&tl->colors.selected.fg, selected_tag_fg);

    rgba_from_str(&tl->colors.urgent.bg, urgent_tag_bg);
    rgba_from_str(&tl->colors.urgent.fg, urgent_tag_fg);

    cr = cairo_create(tw->buffer);
    tl->_lt = pango_cairo_create_layout(cr);
    cairo_destroy(cr);

    pango_layout_set_auto_dir(tl->_lt, true);
    pango_layout_set_font_description(tl->_lt, loft.font.desc);
    pango_layout_set_markup(tl->_lt, m->tags[i].name, -1);
    pango_layout_set_width(tl->_lt, -1);
    pango_layout_get_pixel_size(tl->_lt, &tl->_ext[0], &tl->_ext[1]);

    min_w = tl->_ext[0] + (tag_margins[0] * 2);
    min_h = tl->_ext[1] + (tag_margins[1] * 2);
    pref = MAX(min_w, min_h) + tag_padding;

    loft_connect(&tw->obj, "destroy", tag_label_on_destroy, NULL);

    loft_widget_set_min_size(tw, min_w, min_h);
    loft_widget_set_pref_size(tw, pref, pref);
    loft_widget_attach(tw, &p->lt_tags.w, -1);
  }

  loft_label_init(&p->layout, NULL, false);
  loft_label_init(&p->title, NULL, false);
  loft_label_init(&p->counter, NULL, false);
  loft_label_init(&p->status, NULL, true);

  loft_label_set_padding(&p->layout,  8, 0, 8, 0);
  loft_label_set_padding(&p->title,   8, 0, 8, 0);
  loft_label_set_padding(&p->counter, 8, 0, 8, 0);
  loft_label_set_padding(&p->status,  8, 0, 8, 0);

  loft_label_set_alignment(&p->title, title_alignment);
  loft_label_set_ellipsize(&p->title, title_ellipsize);
  loft_label_set_word_wrap(&p->title, true);

  rgba_from_str(&p->layout.w.colors[NORMAL][IDLE].bg, layout_bg);
  rgba_from_str(&p->layout.w.colors[NORMAL][IDLE].fg, layout_fg);

  rgba_from_str(&p->title.w.colors[NORMAL][IDLE].bg, title_bg);
  rgba_from_str(&p->title.w.colors[NORMAL][IDLE].fg, title_fg);

  rgba_from_str(&p->counter.w.colors[NORMAL][IDLE].bg, counter_bg);
  rgba_from_str(&p->counter.w.colors[NORMAL][IDLE].fg, counter_fg);

  rgba_from_str(&p->status.w.colors[NORMAL][IDLE].bg, status_bg);
  rgba_from_str(&p->status.w.colors[NORMAL][IDLE].fg, status_fg);

  loft_widget_hide(&p->counter.w);
  loft_widget_hide(&p->status.w);

  loft_label_set_draw_bg(&p->layout, true);
  loft_label_set_draw_bg(&p->title, true);
  loft_label_set_draw_bg(&p->counter, true);
  loft_label_set_draw_bg(&p->status, true);

  loft_widget_set_attrs(&p->lt_horiz.w, EXPAND);
  loft_widget_set_attrs(&p->lt_tags.w, EXPAND_Y);
  loft_widget_set_attrs(&p->layout.w, EXPAND_Y);
  loft_widget_set_attrs(&p->title.w, EXPAND);
  loft_widget_set_attrs(&p->counter.w, EXPAND_Y);
  loft_widget_set_attrs(&p->status.w, EXPAND_Y);

  loft_widget_attach(&p->lt_horiz.w, &p->window.w, -1);
  loft_widget_attach(&p->lt_tags.w, &p->lt_horiz.w, -1);
  loft_widget_attach(&p->layout.w, &p->lt_horiz.w, -1);
  loft_widget_attach(&p->title.w, &p->lt_horiz.w, -1);
  loft_widget_attach(&p->counter.w, &p->lt_horiz.w, -1);
  loft_widget_attach(&p->status.w, &p->lt_horiz.w, -1);

  loft_widget_set_pref_size(&p->window.w, m->mw, panel_height);

  loft_connect(&p->window.w.obj, "resize", panel_on_change, m);
  loft_connect(&p->window.w.obj, "set-visible", panel_on_change, m);

  int base_h;
  loft_widget_base_size(&p->window.w, NULL, &base_h);

  int py = p->position == TOP ? m->my : m->my + m->mh - base_h;
  loft_window_move(&p->window, m->mx, py);

  if(panel_visible)
    loft_window_show(&p->window);

  return p;
}

void panel_destroy (Monitor *m) {
  loft_object_destroy(&m->panel->window.w.obj);
  free(m->panel);
}

void panel_refresh (Monitor *m) {
  if(m != NULL)
    _panel_refresh(m);
  else for(m = mons; m != NULL; m = m->next)
    _panel_refresh(m);
}

void panel_sync_counter (Monitor *m) {
  int focused = 0,
      count = 0;

  Client *c;
  for(c = next_visible(m->clients); c != NULL; c = next_visible(c->next)) {
    ++count;
    if(c == m->selected)
      focused = count;
  }

  char buf[16];
  snprintf(buf, 16, counter_format_str, focused, count);
  loft_label_set_str(&m->panel->counter, buf, false);

  if(count > 0)
    loft_widget_show(&m->panel->counter.w);
  else
    loft_widget_hide(&m->panel->counter.w);
}

void panel_sync_layout (Monitor *m) {
  char buf[32];
  snprintf(buf, 32, "%s %s", CURTAGW(m).layout->str, attach_point_sym[m->attach_point]);
  loft_label_set_str(&m->panel->layout, buf, false);
}

void panel_sync_tags (Monitor *m) {
  bool can_indicate = m == selmon && m->selected != NULL;

  int i;
  Tag *t;
  unsigned int tm,
               ps;
  Client *c;

  for(i = 0; i < m->n_tags; ++i) {
    t = &m->tags[i];
    tm = 1 << i;
    ps = t->state;

    t->state = 0;
    if(i == CURTAG(m))
      t->state |= STATE_CURRENT;
    if(tm & SELTAGS(m))
      t->state |= STATE_SELECTED;
    if(can_indicate && (m->selected->tags & tm))
      t->state |= STATE_INDICATE;

    for(c = m->clients; c != NULL; c = c->next) {
      if(c->tags & tm) {
        t->state |= STATE_USED;
        if(c->urgent)
          t->state |= STATE_URGENT;
      }
    }

    if(t->state != ps)
      t->label->w.redraw = true;
  }
}

void panel_sync_title (Monitor *m) {
  char *title;
  if(m->selected == NULL)
    title = NULL;
  else if(m->selected->name[0] != '\0')
    title = m->selected->name;
  else
    title = title_fallback;

  loft_label_set_str(&m->panel->title, title, false);
}

void refresh_status (void) {
  if(xwindow_get_text_prop(root, XA_WM_NAME, status, sizeof(status)) == false)
    snprintf(status, sizeof(status), "<span color='#AAAAAA'>ProperWM</span> <span color='#888888'>%i</span>", VERSION);

  Monitor *m;
  for(m = mons; m != NULL; m = m->next)
    loft_label_set_str(&m->panel->status, status, true);
}

void show_hide_status (void) {
  Monitor *m;
  for(m = mons; m != NULL; m = m->next) {
    if(m != selmon)
      loft_widget_hide(&m->panel->status.w);
    else
      loft_widget_show(&m->panel->status.w);
  }
}

void update_panels (void) {
  Monitor *m;
  for(m = mons; m != NULL; m = m->next) {
    m->panel = panel_create(m);
    panel_refresh(m);
  }
}
