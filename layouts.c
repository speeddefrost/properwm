#define NMASTER(x,max) (x < 0 ? max/2 : (x < max ? x : max-1))

void vtile (Monitor *m) {
  int nv = clients_vt_count(m->clients);
  if(nv == 0)
    return;

  Tag *ct = &CURTAGW(m);

  int nm = NMASTER(ct->nmaster, nv),
      ns = nv - nm;

  int hp = m->padding / 2,  // half
      cp = m->padding + hp, // common
      dp = m->padding * 2;  // double

  int mw=0, mh, mcx, mcy, mcw,
      sh=0, scx, scy=0, scw;

  if(nm > 0) {
    mw = m->ww *ct->mfactor;
    mh = m->wh - (m->padding * (nm + 1));

    mcx = m->wx + m->padding;
    mcy = m->padding;

    scx = m->wx + mw + hp;
    scw = m->ww - mw - cp;
  }
  else {
    scx = m->wx + m->padding;
    scw = m->ww - dp;
  }

  if(ns > 0) {
    sh = m->wh - (m->padding * (ns + 1));

    mcw = mw - cp;
    scy = m->padding;
  }
  else {
    mcw = m->ww - dp;
  }

  Client *c;
  float mf, sf;
  int i, bw2, height;

  for(i=0, mf=0.0, c = nv_tiled(m->clients); i < nm; ++i, c = nv_tiled(c->next))
    mf += c->cfactor;
  for(sf=0.0; i < nv; ++i, c = nv_tiled(c->next))
    sf += c->cfactor;

  for(i=0, c = nv_tiled(m->clients); i < nm; ++i, c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    height = mh * (c->cfactor / mf);
    client_resize(c, mcx, m->wy + mcy, mcw - bw2, height - bw2);
    mcy += HEIGHT(c) + m->padding;
    mh -= HEIGHT(c);
    mf -= c->cfactor;
  }

  for(; i < nv; ++i, c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    height = sh * (c->cfactor / sf);
    client_resize(c, scx, m->wy + scy, scw - bw2, height - bw2);
    scy += HEIGHT(c) + m->padding;
    sh -= HEIGHT(c);
    sf -= c->cfactor;
  }
}

void htile (Monitor *m) {
  int nv = clients_vt_count(m->clients);
  if(nv == 0)
    return;

  Tag *ct = &CURTAGW(m);

  int nm = NMASTER(ct->nmaster, nv),
      ns = nv - nm;

  int hp = m->padding / 2,  // half
      cp = m->padding + hp, // common
      dp = m->padding * 2;  // double

  int mw, mh=0, mcx, mcy, mch,
      sw=0, scx=0, scy, sch;

  if(nm > 0) {
    mw = m->ww - (m->padding * (nm + 1));
    mh = m->wh *ct->mfactor;

    mcx = m->padding;
    mcy = m->wy + m->padding;

    scy = m->wy + mh + hp;
    sch = m->wh - mh - cp;
  }
  else {
    scy = m->wy + m->padding;
    sch = m->wh - dp;
  }

  if(ns > 0) {
    sw = m->ww - (m->padding * (ns + 1));

    mch = mh - cp;
    scx = m->padding;
  }
  else {
    mch = m->wh - dp;
  }

  Client *c;
  float mf, sf;
  int i, bw2, width;

  for(i=0, mf=0.0, c = nv_tiled(m->clients); i < nm; ++i, c = nv_tiled(c->next))
    mf += c->cfactor;
  for(sf=0.0; i < nv; ++i, c = nv_tiled(c->next))
    sf += c->cfactor;

  for(i = 0, c = nv_tiled(m->clients); i < nm; ++i, c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    width = mw * (c->cfactor / mf);
    client_resize(c, m->wx + mcx, mcy, width - bw2, mch - bw2);
    mcx += WIDTH(c) + m->padding;
    mw -= WIDTH(c);
    mf -= c->cfactor;
  }

  for(; i < nv; ++i, c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    width = sw * (c->cfactor / sf);
    client_resize(c, m->wx + scx, scy, width - bw2, sch - bw2);
    scx += WIDTH(c) + m->padding;
    sw -= WIDTH(c);
    sf -= c->cfactor;
  }
}

void vfold (Monitor *m) {
  int nv = clients_vt_count(selmon->clients);
  if(nv == 0)
    return;

  Tag *ct = &CURTAGW(m);

  float hdec = ct->mfactor / nv;
  int p2 = m->padding * 2,
      th = m->wh - p2,
      width = m->ww - p2;

  Client *c;
  float hmult;
  int bw2, height, y;

  for(hmult=1.0, c = nv_tiled(m->clients); c != NULL; hmult -= hdec, c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    height = (th - bw2) * hmult;
    y = (m->wy + m->wh) - m->padding - height - bw2;
    client_resize(c, m->wx + m->padding, y, width - bw2, height);
  }
}

void hfold (Monitor *m) {
  int nv = clients_vt_count(selmon->clients);
  if(nv == 0)
    return;

  Tag *ct = &CURTAGW(m);

  float wdec = ct->mfactor / nv;
  int p2 = m->padding * 2,
      tw = m->ww - p2,
      height = m->wh - p2;

  Client *c;
  float wmult;
  int bw2, width, x;

  for(wmult=1.0, c = nv_tiled(m->clients); c != NULL; wmult -= wdec, c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    width = (tw - bw2) * wmult;
    x = (m->wx + m->ww) - m->padding - width - bw2;
    client_resize(c, x, m->wy + m->padding, width, height - bw2);
  }
}

void monocle (Monitor *m) {
  Client *c;
  int bw2;

  for(c = nv_tiled(m->clients); c != NULL; c = nv_tiled(c->next)) {
    bw2 = c->bw * 2;
    client_resize(c, m->wx, m->wy, m->ww - bw2, m->wh - bw2);
  }
}
