void arrange (Monitor *m) {
  if(m != NULL) {
    show_hide(m);
    monitor_restack(m);
    monitor_arrange(m);
  }
  else {
    for(m = mons; m != NULL; m = m->next) {
      show_hide(m);
      monitor_arrange(m);
    }
  }

  XEvent ev;
  while(XCheckMaskEvent(dpy, EnterWindowMask, &ev));
}

void show_hide (Monitor *m) {
  Client *c;
  for(c = m->stack; c != NULL; c = c->snext) {
    if(ONSCREEN(c))
      XMoveWindow(dpy, c->win, c->x, c->y);
    else
      XMoveWindow(dpy, c->win, (m->wx + c->w) * -1, (m->wy + c->h) * -1);
  }
}

void monitor_arrange (Monitor *m) {
  Client *c;
  Tag *ct = &CURTAGW(m);

  if(ct->layout->func == NULL) {
    for(c = m->clients; c != NULL; c = c->next) {
      if(ONSCREEN(c) == false || c->fullscreen)
        continue;

      client_set_border_width(c, border_width);
      client_resize(c, c->fx, c->fy, c->fw, c->fh);
    }

    return;
  }

  int tiled_bw;
  if(ct->layout->func == monocle || clients_vt_count(m->clients) == 1)
    tiled_bw = 0;
  else
    tiled_bw = border_width;

  for(c = m->clients; c != NULL; c = c->next) {
    if(ONSCREEN(c) == false || c->fullscreen)
      continue;

    // restore borders and geometry

    if(c->floating) {
      client_set_border_width(c, border_width);
      client_resize(c, c->fx, c->fy, c->fw, c->fh);
    }
    else {
      client_set_border_width(c, tiled_bw);
    }
  }

  ct->layout->func(m);
}

Monitor * monitor_create (int index) {
  Monitor *m = malloc(sizeof(Monitor));
  if(m == NULL)
    die("out of memory\n");

  m->index = index;
  m->panel = NULL;

  m->attach_point = init_attach_point;
  memcpy(m->struts, struts, sizeof(int) * 4);

  m->selected = NULL;
  m->clients = NULL;
  m->stack = NULL;

  m->n_tags = LENGTH(tags);
  m->tags = malloc(sizeof(Tag) * m->n_tags);
  if(m->tags == NULL)
    die("out of memory\n");

  int i;
  Tag *t;
  TagTemplate *tpl;

  for(i = 0; i < m->n_tags; ++i) {
    t = &m->tags[i];
    tpl = &tags[i];

    t->index = i;
    t->name = tpl->name;
    t->label = NULL;

    t->selected = NULL;
    t->state = 0;

    t->layout = &layouts[tpl->layout];
    t->mfactor = tpl->mfactor;
    t->nmaster = tpl->nmaster;
  }

  m->padding = padding;

  m->si = 0;
  memcpy(m->curtags, init_current_tags, sizeof(unsigned int) * 2);
  memcpy(m->seltags, init_selected_tags, sizeof(unsigned int) * 2);

  m->seltags[0] |= (1 << m->curtags[0]);
  m->seltags[1] |= (1 << m->curtags[1]);

  m->next = NULL;

  return m;
}

void monitor_restack (Monitor *m) {
  if(m->selected == NULL)
    return;

  XWindowChanges wc = {
    .sibling = m->panel->window.xwin,
    .stack_mode = Below
  };

  if(m->selected->floating || CURTAGW(selmon).layout->func == NULL)
    XRaiseWindow(dpy, m->selected->win);

  Client *c;
  for(c = m->stack; c != NULL; c = c->snext) {
    if(c->floating)
      continue;

    XConfigureWindow(dpy, c->win, CWSibling|CWStackMode, &wc);
    wc.sibling = c->win;
  }
}

void monitor_set_strut (Monitor *m, int strut, int px) {
  if(strut > 3)
    return;

  if(m != NULL) {
    m->struts[strut] = px;
    monitor_update_geometry(m);
    arrange(m);
  }
  else {
    for(m = mons; m != NULL; m = m->next) {
      m->struts[strut] = px;
      monitor_update_geometry(m);
      arrange(m);
    }
  }
}

void monitor_set_tags (Monitor *m, unsigned int tm) {
  tm &= TAG_MASK;
  if(tm == 0 || tm == SELTAGS(m))
    return;

  SELTAGS(m) = tm;

  if((tm & (1 << CURTAG(m))) == 0) {
    int i;
    for(i = 0; i < m->n_tags; ++i) {
      if(tm & (1 << i)) {
        CURTAG(m) = i;
        break;
      }
    }
  }

  focus(CURTAGW(m).selected);
  arrange(m);
}

void monitor_toggle_tags (Monitor *m, unsigned int tm) {
  tm &= TAG_MASK;
  if(tm == 0 || tm == SELTAGS(m))
    return;

  unsigned int ntm = SELTAGS(m) ^ tm;
  SELTAGS(m) = ntm;

  if((ntm & (1 << CURTAG(m))) == 0) {
    int i;
    for(i = 0; i < m->n_tags; ++i) {
      if(ntm & (1 << i)) {
        CURTAG(m) = i;
        break;
      }
    }
  }

  focus(CURTAGW(m).selected);
  arrange(m);
}

void monitor_update_geometry (Monitor *m) {
  int i;
  int px;

  m->wx = m->mx;
  m->wy = m->my;
  m->ww = m->mw;
  m->wh = m->mh;

  for(i = 0; i < 4; ++i) {
    px = m->struts[i];

    if(i == STRUT_TOP) {
      m->wh -= px;
      m->wy += px;
    }
    else if(i == STRUT_BOTTOM) {
      m->wh -= px;
    }
    else if(i == STRUT_LEFT) {
      m->ww -= px;
      m->wx += px;
    }
    else if(i == STRUT_RIGHT) {
      m->ww -= px;
    }
  }
}

Monitor * cycle_monitors (int d) {
  Monitor *m = NULL;

  if(d > 0) {
    m = selmon->next;
    if(m == NULL)
      m = mons;
  }
  else {
    if(selmon == mons)
      for(m = mons; m->next != NULL; m = m->next);
    else
      for(m = mons; m->next != selmon; m = m->next);
  }

  return m;
}

#ifdef XINERAMA
void init_unique_monitors (XineramaScreenInfo *info, int n) {
  mons = monitor_create(0);

  int i;
  Monitor *m;

  for(i = 1, m = mons; i < n && m != NULL; ++i, m = m->next)
    m->next = monitor_create(i);

  for(i = 0, m = mons; i < n && m != NULL; ++i, m = m->next) {
    m->mx = info[i].x_org;
    m->my = info[i].y_org;
    m->mw = info[i].width;
    m->mh = info[i].height;

    monitor_update_geometry(m);
  }
}
#endif

void setup_monitors (void) {
#ifdef XINERAMA
  if(XineramaIsActive(dpy)) {
    int n;
    XineramaScreenInfo *info = XineramaQueryScreens(dpy, &n);
    if(info == NULL)
      die("out of memory\n");

    init_unique_monitors(info, n);
    XFree(info);

    goto sm_finalize;
  }
#endif

  mons = monitor_create(0);
  mons->mx = 0;
  mons->my = 0;
  mons->mw = mons->ww = scr_width;
  mons->mh = mons->wh = scr_height;

  monitor_update_geometry(mons);

sm_finalize:

  selmon = mons;
}

bool update_monitors (void) {
  Monitor *oldmons = mons;

  Monitor *m;
  Client *c;

#ifdef XINERAMA
  if(XineramaIsActive(dpy)) {
    int i,n,u;

    XineramaScreenInfo *info = XineramaQueryScreens(dpy, &n);
    XineramaScreenInfo *unique = malloc(sizeof(XineramaScreenInfo) * n);

    if(info == NULL || unique == NULL)
      die("out of memory\n");

    //  copy unique screens
    for(i = 0, u = 0, m = oldmons; i < n; ++i, m = m->next) {
      if(m->mx != info[i].x_org ||
          m->my != info[i].y_org ||
          m->mw != info[i].width ||
          m->mh != info[i].height)
      {
        memcpy(&unique[u++], &info[i], sizeof(XineramaScreenInfo));
      }
    }

    XFree(info);

    if(u == 0) {
      free(unique);
      return false;
    }

    init_unique_monitors(unique, u);
    free(unique);

    goto um_finalize;
  }
#endif

  if(mons == NULL)
    mons = monitor_create(0);
  else if(scr_width == mons->mw && scr_height == mons->mh)
    return false;

  mons->mx = 0;
  mons->my = 0;
  mons->mw = mons->ww = scr_width;
  mons->mh = mons->wh = scr_height;

um_finalize:

  selmon = mons;

  //  reattach clients and deinit/free previous monitors
  while(oldmons != NULL) {
    m = oldmons;
    oldmons = m->next;

    while(m->clients != NULL) {
      c = m->clients;
      m->clients = c->next;

      c->mon = mons;
      attach_tail(c);
      attach_stack(c);

      show_hide(c->mon);
    }

    panel_destroy(m);
    free(m);
  }

  XSync(dpy, false);
  return true;
}

Monitor * xwindow_to_monitor (Window w) {
  int x, y;
  Client *c;
  Monitor *m;

  if(w == root && get_root_ptr(&x, &y)) {
    return xy_to_monitor(x, y);
  }
  else for(m = mons; m != NULL; m = m->next) {
    if(w == m->panel->window.xwin)
      return m;
  }

  c = xwindow_to_client(w);

  if(c != NULL)
    return c->mon;

  return NULL;
}

Monitor * xy_to_monitor (int x, int y) {
  Monitor *m;
  for(m = mons; m != NULL; m = m->next) {
    if(x >= m->mx && x <= m->mx + m->mw &&
        y >= m->my && y <= m->my + m->mh)
    {
      return m;
    }
  }

  return NULL;
}
