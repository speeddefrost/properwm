#include "properwm.h"
#include "config.h"

#include <linux/limits.h>
#include <unistd.h>
#include <X11/Xatom.h>
#include <X11/cursorfont.h>
#include <X11/Xproto.h>
#include <X11/Xutil.h>

int (*xerrorxlib) (Display*, XErrorEvent*);

void (*handlers[LASTEvent]) (XEvent*) = {
  [ButtonPress] = on_button_press,
  [ClientMessage] = on_client_message,
  [ConfigureNotify] = on_configure_notify,
  [ConfigureRequest] = on_configure_request,
  [DestroyNotify] = on_destroy_notify,
  [EnterNotify] = on_enter_notify,
  [Expose] = on_expose,
  [FocusIn] = on_focus_in,
  [KeyPress] = on_key_press,
  [MappingNotify] = on_mapping_notify,
  [MapRequest] = on_map_request,
  [MotionNotify] = on_motion_notify,
  [PropertyNotify] = on_property_notify,
  [UnmapNotify] = on_unmap_notify
};

char        exec_path[PATH_MAX];
Display     *dpy;
Window      root;

int         screen,
            scr_width,
            scr_height;

struct {
  long border_normal,
       border_previous,
       border_selected,
       border_urgent;
} colors;

Cursor      cursor[CursorLast];
Atom        netatom[NetLast];
Atom        wmatom[WMLast];

uint8_t     numlockmask;

int         ret = 2;
bool        running = false;
Monitor     *mons = NULL;
Monitor     *selmon = NULL;
char        status [512];

#include "actions.c"
#include "client.c"
#include "layouts.c"
#include "main.c"
#include "monitor.c"
#include "panel.c"
#include "xevents.c"
#include "xmisc.c"
#include "xwindow.c"
