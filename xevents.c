#include <X11/XKBlib.h>

void on_button_press (XEvent *e) {
  XButtonPressedEvent *ev = &e->xbutton;
  struct xy pointer = { ev->x, ev->y };
  Client *c = xwindow_to_client(ev->window);
  Monitor *m = c != NULL ? c->mon : xy_to_monitor(ev->x_root, ev->y_root);

  int click;

  if(c != NULL) {
    if(ev->button == 1)
      focus(c);

    monitor_restack(c->mon);
    panel_refresh(c->mon);
    click = CLICK_CLIENT;
  }
  else {
    if(m != NULL && m != selmon) {
      if(selmon->selected != NULL)
        unfocus(selmon->selected);

      reset_focus();

      selmon = m;
      focus(NULL);
      show_hide_status();
    }

    if(ev->window == selmon->panel->window.xwin) {
      Panel *p = selmon->panel;
      LoftWidget *w = loft_widget_get_child_at_xy(&p->lt_horiz.w, &pointer);

      if(w == &p->lt_tags.w)
        click = CLICK_TAG;
      else if(w == &p->layout.w)
        click = CLICK_LAYOUT;
      else if(w == &p->title.w)
        click = CLICK_TITLE;
      else if(w == &p->counter.w)
        click = CLICK_COUNTER;
      else if(w == &p->status.w)
        click = CLICK_STATUS;
      else
        click = CLICK_ROOT;
    }
    else {
      click = CLICK_ROOT;
    }
  }

  int i;
  if(click == CLICK_TAG) {
    TagLabel *tl = (TagLabel*) loft_widget_get_child_at_xy(&m->panel->lt_tags.w, &pointer);
    if(tl == NULL)
      return;

    for(i = 0; i < LENGTH(buttons); ++i) {
      if(buttons[i].click == click &&
         buttons[i].button == ev->button &&
         buttons[i].arg.ui == (1 << tl->tag->index) &&
         CLEAN_MASK(buttons[i].mask) == CLEAN_MASK(ev->state))
      {
        buttons[i].func(&buttons[i].arg);
        break;
      }
    }
  }
  else {
    for(i = 0; i < LENGTH(buttons); ++i) {
      if(buttons[i].click == click &&
         buttons[i].button == ev->button &&
         CLEAN_MASK(buttons[i].mask) == CLEAN_MASK(ev->state))
      {
        buttons[i].func(&buttons[i].arg);
        break;
      }
    }
  }
}

void on_client_message (XEvent *e) {
  XClientMessageEvent *cme = &e->xclient;
  Client *c = xwindow_to_client(cme->window);
  if(c == NULL)
    return;

  if(cme->message_type == netatom[NetActiveWindow]) {
    if(ONSCREEN(c) == false)
      return;

    focus(c);
    panel_refresh(c->mon);
  }
  else if(cme->message_type != netatom[NetWMState]) {
    if(cme->data.l[1] == netatom[NetWMFullscreen] || cme->data.l[2] == netatom[NetWMFullscreen])
      client_set_fullscreen(c, (cme->data.l[0] == 1 || cme->data.l[0] == 2) && c->fullscreen == false);
    //                              _NET_WM_STATE_ADD   _NET_WM_STATE_TOGGLE
  }
}

void on_configure_notify (XEvent *e) {
  XConfigureEvent *cn = &e->xconfigure;
  if(cn->window != root)
    return;

  bool dirty = cn->width != scr_width || cn->height != scr_height;

  scr_width = cn->width;
  scr_height = cn->height;

  if(update_monitors() || dirty) {
    update_panels();
    refresh_status();
    show_hide_status();

    focus(NULL);
    arrange(NULL);
  }
}

void on_configure_request (XEvent *e) {
  XConfigureRequestEvent *ev = &e->xconfigurerequest;
  Client *c = xwindow_to_client(ev->window);

  if(c != NULL) {
    if(ev->value_mask & CWBorderWidth)
      c->bw = ev->border_width;

    if(c->floating || CURTAGW(c->mon).layout->func == NULL) {
      Monitor *m = c->mon;

      if(ev->value_mask & CWX)
        c->x = m->mx + ev->x;
      if(ev->value_mask & CWY)
        c->y = m->my + ev->y;
      if(ev->value_mask & CWWidth)
        c->w = ev->width;
      if(ev->value_mask & CWHeight)
        c->h = ev->height;

      if(c->fullscreen == false) {
        c->fx = c->x;
        c->fy = c->y;
        c->fw = c->w;
        c->fh = c->h;
      }

      client_configure(c);

      if(ONSCREEN(c))
        XMoveResizeWindow(dpy, c->win, c->x, c->y, c->w, c->h);
    }
    else {
      client_configure(c);
    }
  }
  else {
    XWindowChanges wc = {
      .border_width = ev->border_width,
      .x = ev->x,
      .y = ev->y,
      .width = ev->width,
      .height = ev->height,
      .sibling = ev->above,
      .stack_mode = ev->detail
    };

    XConfigureWindow(dpy, ev->window, ev->value_mask, &wc);
  }

  XSync(dpy, false);
}

void on_destroy_notify (XEvent *e) {
  XDestroyWindowEvent *ev = &e->xdestroywindow;
  Client *c = xwindow_to_client(ev->window);
  if(c != NULL)
    client_unmanage(c, true);
}

void on_enter_notify (XEvent *e) {
  if(focus_follows_mouse == false)
    return;

  XCrossingEvent *ev = &e->xcrossing;
  Client *c = xwindow_to_client(ev->window);

  if(c != NULL && c != selmon->selected) {
    focus(c);
    monitor_restack(c->mon);
    panel_refresh(c->mon);
    return;
  }

  Monitor *m = c != NULL ? c->mon : xwindow_to_monitor(ev->window);
  if(m != NULL && m != selmon) {
    unfocus(selmon->selected);
    reset_focus();

    selmon = m;
    show_hide_status();
    focus(CURTAGW(selmon).selected);
    monitor_restack(selmon);
    panel_refresh(selmon);
  }
}

void on_expose (XEvent *e) {
  return;
}

void on_focus_in (XEvent *e) {
  if(selmon->selected == NULL)
    return;

  XFocusChangeEvent *ev = &e->xfocus;
  Client *c = xwindow_to_client(ev->window);
  if(c != NULL && c != selmon->selected)
    set_focus(selmon->selected);
}

void on_key_press (XEvent *e) {
  XKeyEvent *ev = &e->xkey;
  KeySym keysym = XkbKeycodeToKeysym(dpy, (KeyCode) ev->keycode, 0, 0);

  int i;
  for(i = 0; i < LENGTH(keys); ++i) {
    if(keysym == keys[i].keysym &&
       CLEAN_MASK(keys[i].mask) == CLEAN_MASK(ev->state))
    {
      keys[i].function(&keys[i].arg);
    }
  }
}

void on_map_request (XEvent *e) {
  static XWindowAttributes wa;
  XMapRequestEvent *ev = &e->xmaprequest;

  if(XGetWindowAttributes(dpy, ev->window, &wa) == false)
    return;

  if(wa.override_redirect)
    return;

  if(xwindow_to_client(ev->window) == NULL)
    xwindow_manage(ev->window, &wa);
}

void on_mapping_notify (XEvent *e) {
  XMappingEvent *ev = &e->xmapping;
  XRefreshKeyboardMapping(ev);

  if(ev->request == MappingKeyboard)
    grab_keys();
}

void on_motion_notify (XEvent *e) {
  if(focus_follows_mouse == false || e->xmotion.window != root)
    return;

  static Monitor *mon = NULL;
  Monitor *m = xy_to_monitor(e->xmotion.x_root, e->xmotion.y_root);

  if(m != mon && mon != NULL) {
    unfocus(selmon->selected);
    reset_focus();

    selmon = m;
    show_hide_status();
    focus(CURTAGW(selmon).selected);
    panel_refresh(selmon);
  }

  mon = m;
}

void on_property_notify (XEvent *e) {
  XPropertyEvent *ev = &e->xproperty;

  if(ev->window == root && ev->atom == XA_WM_NAME) {
    refresh_status();
    return;
  }

  if(ev->state == PropertyDelete)
    return;

  Client *c = xwindow_to_client(ev->window);
  if(c == NULL)
    return;

  if(ev->atom == XA_WM_TRANSIENT_FOR) {
    Window trans;
    if(c->floating == false && XGetTransientForHint(dpy, c->win, &trans) && xwindow_to_client(trans) != NULL) {
      c->floating = true;
      if(c == selmon->selected)
        focus(NULL);
      arrange(c->mon);
    }
  }
  else if(ev->atom == XA_WM_NORMAL_HINTS) {
    client_update_size_hints(c);
    if(c->floating) {
      int diff = (c->min_w > c->w) | ((c->min_h > c->h) << 1);
      if(diff) {
        client_resize (
          c, c->x, c->y,
          diff & 1 ? c->min_w : c->w,
          diff & 2 ? c->min_h : c->h
        );
      }
    }
  }
  else if(ev->atom == XA_WM_HINTS) {
    client_update_wm_hints(c);
  }
  else if(ev->atom == netatom[NetWMWindowType]) {
    client_update_window_type(c);
  }
  else if(ev->atom == XA_WM_NAME || ev->atom == netatom[NetWMName]) {
    client_update_name(c);
    if(c == c->mon->selected)
      panel_refresh(c->mon);
  }
}

void on_unmap_notify (XEvent *e) {
  XUnmapEvent *ev = &e->xunmap;
  Client *c = xwindow_to_client(ev->window);

  if(c != NULL) {
    if(ev->send_event)
      client_set_state(c, WithdrawnState);
    else
      client_unmanage(c, false);
  }
}
